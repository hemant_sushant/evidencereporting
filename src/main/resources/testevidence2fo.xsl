
<!--

  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<!--  $Id$  -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.1" exclude-result-prefixes="fo">
    <xsl:output method="xml" version="1.0" omit-xml-declaration="no" indent="yes"/>
    <xsl:param name="versionParam" select="'1.0'"/>
    <!--  =========================  -->
    <!--  root element: testCaseEvidence  -->
    <!--  =========================  -->
    <xsl:template match="testCaseEvidence">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="simpleA4" page-height="29.7cm" page-width="21cm" margin-top="0.5cm" margin-bottom="1cm" margin-left="1.5cm" margin-right="1.5cm">
                    <fo:region-body region-name="xsl-region-body" margin-bottom=".5in" margin-top="0.9in"/>
                    <fo:region-before region-name="xsl-region-before" extent="1.5in"/>
                    <fo:region-after region-name="xsl-region-after" extent=".5in"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="simpleA4">
                <!--  ============================== -->
                <!--  Content: Header          -->
                <!--  ============================== -->
                <fo:static-content flow-name="xsl-region-before">
                    <fo:block>
                        <fo:table table-layout="fixed" width="100%" border-collapse="separate" border-separation="1pt">
                            <fo:table-column column-width="25%"/>
                            <fo:table-column column-width="50%"/>
                            <fo:table-column column-width="25%"/>
                            <fo:table-body>
                                <fo:table-row>
                                    <fo:table-cell display-align="after">
                                        <fo:block>
                                            <fo:external-graphic width="100%" height="1.5cm" scaling="uniform" scaling-method="auto" content-width="scale-down-to-fit" content-height="scale-down-to-fit">
                                                <xsl:attribute name="src">
                                                    <xsl:text>url('data:image/jpeg;base64,</xsl:text>
                                                    <xsl:value-of select="clientImage"/>
                                                    <xsl:text>')</xsl:text>
                                                </xsl:attribute>
                                            </fo:external-graphic>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell display-align="after">
                                        <fo:block font-size="20pt" text-align="center">
                                            <xsl:value-of select="projectName"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell display-align="after">
                                        <fo:block text-align="right">
                                            <fo:external-graphic width="100%" height="1.5cm" scaling="uniform" scaling-method="auto" content-width="scale-down-to-fit" content-height="scale-down-to-fit">
                                                <xsl:attribute name="src">
                                                    <xsl:text>url('data:image/jpeg;base64,</xsl:text>
                                                    <xsl:value-of select="companyImage"/>
                                                    <xsl:text>')</xsl:text>
                                                </xsl:attribute>
                                            </fo:external-graphic>
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </fo:table-body>
                        </fo:table>
                    </fo:block>
                    <fo:block>
                        <fo:leader leader-pattern="rule" rule-thickness="2.0pt" leader-length="100%"/>
                    </fo:block>
                </fo:static-content>
                <!--  ============================== -->
                <!--  Content: Footer          -->
                <!--  ============================== -->
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block>
                        <fo:leader leader-pattern="rule" rule-thickness="1.0pt" leader-length="100%"/>
                    </fo:block>
                    <fo:block text-align="center">
                        Page <fo:page-number/> of
                        <fo:page-number-citation
                                ref-id="TheVeryLastPage"/>
                    </fo:block>
                </fo:static-content>
                <!--  ============================== -->
                <!--  Content: main body             -->
                <!--  ============================== -->
                <fo:flow flow-name="xsl-region-body">
                    <fo:block font-size="10pt" linefeed-treatment="preserve" space-after="5mm">
                        <xsl:text>&#xA;</xsl:text>
                        <fo:table table-layout="fixed" width="100%" border-collapse="separate">
                            <fo:table-column column-width="60%"/>
                            <fo:table-column column-width="40%"/>
                            <fo:table-body>
                                <fo:table-row>
                                    <fo:table-cell>
                                        <fo:block>
                                            <fo:inline font-weight="bold"><xsl:text>TestCase Name: </xsl:text></fo:inline>
                                            <xsl:value-of select="testName"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                        <fo:block>
                                            <fo:inline font-weight="bold"><xsl:text>TestCase Owner: </xsl:text></fo:inline>
                                            <xsl:value-of select="testOwner"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell>
                                        <fo:block>
                                            <fo:inline font-weight="bold"><xsl:text>Execution Date: </xsl:text></fo:inline>
                                            <xsl:value-of select="testDate"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                        <fo:block font-weight="bold">
                                            <fo:inline><xsl:text>TestCase Status: </xsl:text></fo:inline>
                                            <xsl:if test="exceptionMessage = ''">
                                                <fo:inline color="green">Pass</fo:inline>
                                            </xsl:if>
                                            <xsl:if test="exceptionMessage != ''">
                                                <fo:inline color="red">Fail</fo:inline>
                                            </xsl:if>
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </fo:table-body>
                        </fo:table>
                    </fo:block>
                    <fo:block/>
                    <fo:block font-size="10pt" break-after="page">
                        <xsl:apply-templates select="seleniumEvidence"/>
                    </fo:block>
                    <fo:block color="red">
                        <xsl:if test="exceptionMessage != ''">
                            <fo:inline font-weight="bold"><xsl:text>TestCase failed with the following exception: </xsl:text></fo:inline>
                            <xsl:value-of select="exceptionMessage"/>
                        </xsl:if>
                    </fo:block>
                    <fo:block id="TheVeryLastPage"/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    <!--  ============================== -->
    <!--  child element: seleniumEvidence-->
    <!--  ============================== -->
    <xsl:template match="seleniumEvidence">
        <fo:block>
            <xsl:value-of select="evidenceDescription"/>
        </fo:block>
        <fo:block space-after="5mm">
            <fo:external-graphic width="100%" content-width="scale-down-to-fit">
                <xsl:attribute name="src">
                    <xsl:text>url('data:image/jpeg;base64,</xsl:text>
                    <xsl:value-of select="evidence"/>
                    <xsl:text>')</xsl:text>
                </xsl:attribute>
            </fo:external-graphic>
        </fo:block>
    </xsl:template>
</xsl:stylesheet>