package com.test.embedding.tools;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Hemant Sushant on 24/07/2016.
 */
public class PropertiesReader {
    Properties prop = new Properties();
    public PropertiesReader(String fileName) {
        InputStream input = null;
        try{
            input = PropertiesReader.class.getClassLoader().getResourceAsStream(fileName);
            if(input==null){
                System.out.println("Unable to find configuration required - " + fileName);
                return;
            }

            //load a properties file from class path, inside static method
            prop.load(input);
        }catch (IOException ex){
            ex.printStackTrace();
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getProperty(String propertyName){
        return prop.getProperty(propertyName);
    }
}
