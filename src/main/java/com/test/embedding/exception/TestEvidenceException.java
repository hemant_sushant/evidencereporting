package com.test.embedding.exception;

/**
 * Created by Hemant Sushant on 24/07/2016.
 */
public class TestEvidenceException extends Exception {
    public TestEvidenceException () {

    }

    public TestEvidenceException (String message) {
        super (message);
    }

    public TestEvidenceException (Throwable cause) {
        super (cause);
    }

    public TestEvidenceException (String message, Throwable cause) {
        super (message, cause);
    }
}