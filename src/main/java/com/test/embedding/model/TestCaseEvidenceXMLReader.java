/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* $Id$ */

package com.test.embedding.model;

//Java
import java.util.Iterator;
import java.io.IOException;

//SAX
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.test.embedding.tools.AbstractObjectReader;

/**
 * XMLReader implementation for the TestCaseEvidence class. This class is used to
 * generate SAX events from the TestCaseEvidence class.
 */
public class TestCaseEvidenceXMLReader extends AbstractObjectReader {

    /**
     * @see org.xml.sax.XMLReader#parse(InputSource)
     */
    public void parse(InputSource input) throws IOException, SAXException {
        if (input instanceof TestCaseEvidenceInputSource) {
            parse(((TestCaseEvidenceInputSource)input).getTestCaseEvidence());
        } else {
            throw new SAXException("Unsupported InputSource specified. "
                    + "Must be a TestCaseEvidenceInputSource");
        }
    }


    /**
     * Starts parsing the TestCaseEvidence object.
     * @param testCaseEvidence The object to parse
     * @throws SAXException In case of a problem during SAX event generation
     */
    public void parse(TestCaseEvidence testCaseEvidence) throws SAXException {
        if (testCaseEvidence == null) {
            throw new NullPointerException("Parameter testCaseEvidence must not be null");
        }
        if (handler == null) {
            throw new IllegalStateException("ContentHandler not set");
        }

        //Start the document
        handler.startDocument();

        //Generate SAX events for the ProjectTeam
        generateFor(testCaseEvidence);

        //End the document
        handler.endDocument();
    }


    /**
     * Generates SAX events for a TestCaseEvidence object.
     * @param testCaseEvidence TestCaseEvidence object to use
     * @throws SAXException In case of a problem during SAX event generation
     */
    protected void generateFor(TestCaseEvidence testCaseEvidence) throws SAXException {
        if (testCaseEvidence == null) {
            throw new NullPointerException("Parameter testCaseEvidence must not be null");
        }
        if (handler == null) {
            throw new IllegalStateException("ContentHandler not set");
        }

        handler.startElement("testCaseEvidence");
        handler.element("projectName", testCaseEvidence.getProjectName());
        handler.element("clientImage", testCaseEvidence.getClientImage());
        handler.element("companyImage", testCaseEvidence.getCompanyImage());
        handler.element("testDate", testCaseEvidence.getTestDate());
        handler.element("testName", testCaseEvidence.getTestName());
        handler.element("testOwner", testCaseEvidence.getTestOwner());
        handler.element("exceptionMessage", testCaseEvidence.getExceptionMessage());
        Iterator i = testCaseEvidence.getSeleniumEvidences().iterator();
        while (i.hasNext()) {
            SeleniumEvidence seleniumEvidence = (SeleniumEvidence) i.next();
            generateFor(seleniumEvidence);
        }
        handler.endElement("testCaseEvidence");
    }

    /**
     * Generates SAX events for a SeleniumEvidence object.
     * @param seleniumEvidence SeleniumEvidence object to use
     * @throws SAXException In case of a problem during SAX event generation
     */
    protected void generateFor(SeleniumEvidence seleniumEvidence) throws SAXException {
        if (seleniumEvidence == null) {
            throw new NullPointerException("Parameter seleniumEvidence must not be null");
        }
        if (handler == null) {
            throw new IllegalStateException("ContentHandler not set");
        }

        handler.startElement("seleniumEvidence");
        handler.element("evidenceDescription", seleniumEvidence.getEvidenceDescription());
        handler.element("evidence", seleniumEvidence.getEvidence());
        handler.endElement("seleniumEvidence");
    }

}