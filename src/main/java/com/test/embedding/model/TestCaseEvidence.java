package com.test.embedding.model;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import com.test.embedding.exception.TestEvidenceException;
import com.test.embedding.tools.PropertiesReader;
import org.apache.commons.io.FileUtils;

/**
 * Created by Hemant Sushant on 22/07/2016.
 */
public class TestCaseEvidence {

    private String projectName;
    private String companyImage;
    private String clientImage;
    private String testName;
    private String testOwner;
    private String testDate;
    private String exceptionMessage;
    private LinkedList<SeleniumEvidence> seleniumEvidences;

    public String getProjectName() {

        return projectName;
    }

    public String getCompanyImage() {

        return companyImage;
    }

    public String getClientImage() {

        return clientImage;
    }

    public String getTestName() {

        return testName;
    }

    public String getTestOwner() {

        return testOwner;
    }

    public String getTestDate() {

        return testDate;
    }

    public String getExceptionMessage() {

        return exceptionMessage;
    }

    public TestCaseEvidence(String testName, String testOwner, String exceptionMessage,LinkedList<SeleniumEvidence> seleniumEvidences) throws TestEvidenceException {
        final String CONFIGFILENAME="testEvidenceConfig.properties";
        PropertiesReader propertiesReader=new PropertiesReader(CONFIGFILENAME);
        this.projectName=propertiesReader.getProperty("project.name");
        this.companyImage = getEncodedImageFromFile(propertiesReader.getProperty("company.image"));
        this.clientImage = getEncodedImageFromFile(propertiesReader.getProperty("client.image"));
        this.testName = testName;
        this.testOwner = testOwner;
        this.exceptionMessage=exceptionMessage;
        this.seleniumEvidences=seleniumEvidences;
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        //get current date time with Date()
        Date date = new Date();
        this.testDate = dateFormat.format(date);
    }

    public LinkedList<SeleniumEvidence> getSeleniumEvidences() {

        return seleniumEvidences;
    }

    /**
     * Returns a Source object for this object so it can be used as input for
     * a JAXP transformation.
     * @return Source The Source object
     */
    public Source getSourceForTestCaseEvidence() {
        return new SAXSource(new TestCaseEvidenceXMLReader(),
                new TestCaseEvidenceInputSource(this));
    }

    private String getEncodedImageFromFile(String fileName) throws TestEvidenceException {
        URI filePath;
        try{
            filePath = getClass().getClassLoader().getResource("images/"+fileName).toURI();
        }catch (NullPointerException npe){
            throw new TestEvidenceException("File "+fileName+" not found");
        }catch (URISyntaxException ue){
            throw new TestEvidenceException("File "+fileName+" not found with invalid URI");
        }

        File file = new File(filePath);
        byte[] bytes;
        try{
            bytes = FileUtils.readFileToByteArray(file);
        }catch (IOException ioe){
            throw new TestEvidenceException("File "+fileName+" could not be converted to byte string",ioe);
        }
        return Base64.encode(bytes);
    }
}
