package com.test.embedding.model;

/**
 * Created by Hemant Sushant on 22/07/2016.
 */
public class SeleniumEvidence {

    private String evidence;
    private String evidenceDescription;

    public SeleniumEvidence(String evidence, String evidenceDescription) {
        this.evidence = evidence;
        this.evidenceDescription = evidenceDescription;
    }

    public String getEvidence() {
        return evidence;
    }

    public String getEvidenceDescription() {
        return evidenceDescription;
    }
}
